import os, requests, datetime, threading
from time import sleep


def os_print(text):
    os.system('echo {text}'.format(text=text))


def send_signal(is_beacon=False):
    with open('id', 'r') as f:
        worker_name = f.read()
    if is_beacon:
        url = 'http://deepnet.duckdns.org:1785/cms/beacon/'
    else:
        url = 'http://deepnet.duckdns.org:1785/cms/'
    #try:
    #    requests.post(url, {'worker': worker_name})
    #except:
    #    pass


class WatchDog():
    def __init__(self, interval):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.start()

    def run(self):
        sleep(self.interval)
        os_print('WatchDog - {dt}'.format(dt=datetime.datetime.now()))
        os.system('killall minergate-cli')
        send_signal(is_beacon=False)


class Beacon():
    def __init__(self, interval):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        while (True):
            os_print('{dt}'.format(dt=datetime.datetime.now()))
            send_signal(is_beacon=True)
            sleep(self.interval)


def main():
    Beacon(60)  # 1 min
    WatchDog(5400)  #5400 1.5 h
    os.system('minergate-cli -user khu12@asia.com xmr 1')
    os_print('FINISH!!!')


if __name__ == '__main__':
    main()
